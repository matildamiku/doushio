Imageboard em tempo real. Versão [tripchanesca](https://tripchan.moe/att/). 
Fork do original [lalcmellkmal/doushio](https://github.com/lalcmellkmal/doushio)

**Instalação:**

* `npm install`
* `node server/server.js`
* ???????
* PROFIT!

**Produção:**

* aquele esquema do nginx

**Dependências:**

* ImageMagick
* libpng
* node.js + npm
* redis
* ffmpeg 2.2+ ao usar webums

**Scripts:**

* archive/daemon.js - arquiva threads
* upkeep/clean.js - deleta imagens do arquivo

**Submódulos:**

são privados, use `./mod.sh` para obtê-los, dê `cd dconfig` e `./install.sh`
para copiá-los
